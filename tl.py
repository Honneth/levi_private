
import cv2
import time

def cd(t):
    while t:
        mins, secs = divmod(t, 60)
        timer = '{:02d}:{:02d}'.format(mins, secs)
        print(timer, end="\r")
        time.sleep(1)
        t -= 1
    print('Timer up!')

print('setting up camera')
cap = cv2.VideoCapture(0) #æetter forbindelse til computerens defualt kamera kamera 0
print('done')
print('')

t = int(input('Antal sekunder delay: '))
nummer = 1

while True:
    print('reading frame')
    ret, frame = cap.read()

    #timestamp
    ts = time.asctime(time.localtime(time.time()))
    ts1 = ts.translate({ord(i): None for i in ':'})
    filename = str(nummer) + ' ' + str(ts1) + '.jpg'
    nummer += 1
    print('writing frame')    
    cv2.imwrite(filename, frame)

    print('sleeping')
    cd(t)